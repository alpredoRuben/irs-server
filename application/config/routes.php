<?php defined('BASEPATH') OR exit('No direct script access allowed');


/** API SERVICE TO GET OPERATOR AND PRODUCT PULSA */
$route['list/product/all/operator'] = 'services/IRSProduct/postListPulsaAllOperator';
$route['list/product/by/operator']  = 'services/IRSProduct/postListPulsaByOperator';
$route['list/all/operator']         = 'services/IRSOperator/postListAllOperator';
$route['list/operator/product']     = 'services/IRSOperator/postListOperatorProduct';
$route['list/product/by/product_id']  = 'services/IRSProduct/postListProductByCode';
$route['list/product/by/category/name']  = 'services/IRSProduct/postListProductByCategoryName';
$route['list/product/by/category/and/operator/name'] = 'services/IRSProduct/postListAllProductByCategoryAndOperator';

$route['irs/api/secret/key']        = 'services/IRSAuthorize/getEncryptionAPISecretKeyIRS';
$route['irs/api/secret/key/(:any)'] = 'services/IRSAuthorize/getDecryptionAPISecretKeyIRS/$1';


/** IRS API SERVICE TO REQUEST PAYMENT TRANSACTION AND STATUS TRANSACTION  */
$route['request/payment/transaction']                       = 'services/IRSTransaction/postHTTPPaymentTransaction';

$route['default_controller']    = "" ;
$route['404_override'] 	        = '';
$route['translate_uri_dashes'] 	= TRUE;
