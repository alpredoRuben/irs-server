<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Irs extends CI_Controller
{
    protected $irs_id       = "PA0001";
    protected $irs_pin      = "1234";
    protected $irs_uname    = "8F2199";
    protected $irs_pwd      = "BFC5E7";
    protected $ip_send      = "10.16.1.44:9090";
    protected $method_name  = array(
        0 => "topUpRequest",
        1 => "INQ",
        2 => "PAY"
    );


    public $pulsa = array(
        "telkomsel" => array(68),
        "xl"        => array(8, 34),
        "axis"      => array(24),
        "esia"      => array(9),
        "indosat"   => array(32),
        "ceria"     => array(14),
        "flexy"     => array(6),
        "smartfren" => array(25),
        "three"     => array(10, 84)
    );


    public $paket_data = array(
        "telkomsel" => array(79),
        "xl"        => array(83,107),
        "axis"      => array(109),
        "indosat"   => array(104),
        "bolt"      => array(81),
        "smart"     => array(108),
        "three"     => array(85, 105)
    );

    protected $arguments = 'PAYINDO_IRS';
    protected $input_json = null;
    public $security = null;

    public function __construct()
    {
        // parent::__construct();
        $this->security = new Encryption();
        $CI =& get_instance();
        // $CI->load->library('output');
    }

    /** Generate Function JSON Format Input */
    public function setJSONInput() {
        $this->input_json = json_decode(file_get_contents('php://input'), true);
    }

    public function getJSONInput()
    {
        return $this->input_json;
    }

    /** Generate Assigner Input */
    public function getOperatorId() 
    {
        return $this->getJSONInput()['operator_id'];
    }

    public function getOperatorName()
    {
        return $this->getJSONInput()['operator_name'];
    }

    public function getProductCode() 
    {
        return $this->getJSONInput()['product_code'];
    }

    public function getCustomerNumber()
    {
        return $this->getJSONInput()['customer_number'];
    }

    public function getTransactionId()
    {
        return $this->getJSONInput()['transaction_id'];
    }

    public function getMethodCode()
    {
        return $this->getJSONInput()['method_code'];
    }

    public function getSecretKey() {
        return $this->getJSONInput()['secret_key'];
    }

    public function getCategoryId()
    {
        return $this->getJSONInput()['category_id'];
    }

    public function getCategoryName()
    {
        return $this->getJSONInput()['category_name'];
    }

    /** Validate Secret Key */
    public function getValidateSecretKey($secret)
    {
        $secret = $this->getDecrypt($secret);
        return (strcmp($secret, $this->arguments) == 0) ? true : false;
    }

    /** Encrypt String Arguments */
    private function getEncrypt($plantext) 
    {   
        return $this->security->Encrypt($plantext);
    }

    /** Decrypt String Encrypt String Arguments */
    private function getDecrypt($encrypt) 
    {
        return $this->security->Decrypt($encrypt);
    }

    /** Generate API Secret As Secret Key Encryption*/
    public function getEncryptApiSecret()
    {
        return $this->getEncrypt($this->arguments);
    }

    /** Generate API Secret As Secret Key Decryption */
    public function getDecryptApiSecret($decrypt)
    {
        return $this->getDecrypt($decrypt);
    }

    /** Function To Prepare Array Api Builder As Array Data To Request From HTTP Get */
    protected function getApiBuilder($no, $code, $trxid)
    {
        $data = array(
            'id'         => $this->irs_id,
            'pin'        => $this->irs_pin,
            'user'       => $this->irs_uname,
            'pass'       => $this->irs_pwd,
            'kodeproduk' => $code,
            'tujuan'     => $no,
            'counter'    => 1,
            'idtrx'      => $trxid 
        );
        return $data;
    }

    /** Function To Prepare XML Data Array */
    protected function XMLPostData($no, $code)
    {
        $data = array(
            "MSISDN" => "082167107895", 
            "REQUESTID" => $this->irs_id,
            "PIN" => $this->irs_pin,
            "NOHP" => $no,
            "NOM" => $code
        );
        return $data;
    }

    /** Function To Format XML RPC Encode From Array Data Input */
    protected function XMLFormatEncode($method, $data)
    {
        $format = xmlrpc_encode_request($method, $data);
        return $format;
    }

    /** Function To Get Api HTTP Get Url */
    protected function getApiUrl($no, $code, $trxid)
    {
        $builder = $this->getApiBuilder($no, $code, $trxid);
        $api_url = "http://".$this->ip_send."/webportal/api/h2h?".http_build_query($builder);
        return $api_url;
    }

    /** Function To Get Api XML Url */
    protected function getApiXML()
    {
        return "http://".$this->ip_send."/webportal/api/h2hxml";
    }

    /** Function To Set Response JSON From Data Array */
    public function setResponse($status=null, $data=null,  $optional=null, $code=200)
    {
        $array = array(
            "success"  => ($status != null) ? $status : false,
            "messages" => ($data != null) ? $data : "No Response Data (NULL)"
        );

        if($optional != null)
            $array["optional"] = $optional;
        
        return $CI->output->set_content_type('application/json')->set_status_header($code)->set_output(json_encode($array));
    }

    /** Function To Execute Request Transasction From HTTP Get */
    public function HttpGetTransaction($no, $code, $trxid, $timeout=null)
    {
        $irs_url  = $this->getApiUrl($no, $code, $trxid);
        $response = HttpGetCurl($irs_url, $timeout);
        return $response;
    }

    /** Function To Execute Request Post Transaction From HTTP Post XML */
    public function HttpXMLPostTransaction($no, $code, $type)
    {
        $method     = $this->method_name[intval($type)];
        $data_xml   = $this->XMLPostData($no, $code);
        $format_xml = $this->XMLFormatEncode($method, $data_xml); 
        $url_xml    = $this->getApiXML();
        $response   = PostXMLCurl($url_xml, $format_xml);
        return $response;
    }
    
}

/* End of file IRS_api.php */
