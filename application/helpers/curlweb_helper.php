<?php  defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists("HttpGetCurl")){

    function HttpGetCurl($url, $timeout=null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        if($timeout != null){
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        } 
        $response =  curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response, true); 
        return($response);
    }
}

if(!function_exists("HttpPostXMLCurl")){

    function HttpPostXMLCurl($url, $input_xml)
    {
        $ch = curl_init(); // initialize curl handle 
        curl_setopt($ch, CURLOPT_URL, $url); // set url to post to 
        curl_setopt($ch, CURLOPT_VERBOSE, 1); // set url to post to 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable 
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $input_xml); // add POST fields 
        curl_setopt($ch, CURLOPT_POST, 1);
        $response =  curl_exec($ch);
        curl_close($ch);
        $response = json_decode(json_encode(simplexml_load_string($response)), true);
        return $response['params']['param']['value']['struct']['member'];
    }
}


if(!function_exists("PostXMLCurl")){
    function PostXMLCurl($url, $xmldata)
    {
        $req = curl_init($url);
        
        $headers = array();
        array_push($headers,"Content-Type: text/xml");
        array_push($headers,"Content-Length: ".strlen($xmldata));
        array_push($headers,"\r\n");

        curl_setopt($req, CURLOPT_URL, $url);
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($req, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($req, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($req, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($req, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($req, CURLOPT_POSTFIELDS, $xmldata);

        $response = curl_exec($req);
        curl_close($req);
        return $response;
        //return xmlrpc_decode($response);
    }
}