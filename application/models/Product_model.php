<?php  defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . "./models/IRSConfig_model.php";

class Product_model extends IRSConfig_model
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getListProductInAllOperatorByCategory($category_id)
    {
        $category = $this->category[$category_id];
        $record = array();
        foreach ($category as $cID => $cValue) {
            $record[$cID] = array();

            foreach ($cValue  as $operator_id) {
                $nominal = $this->getGroupNominalPulsaByOperatorId($operator_id);

                $temporary = array();
                if(is_array($nominal) && count($nominal) > 0){
                    foreach ($nominal as $fillNominal) {
                        $tmp = $this->getRecordPulsaByOperatorAndNominalGroup($operator_id, $fillNominal['nominal']);
                        if(is_array($tmp) && count($tmp) > 0)
                        {
                            foreach ($tmp as $a) {
                                array_push($temporary, $a);
                            }
                        }
                        else
                            array_push($temporary, $tmp);
                    }
                }
                $record[$cID] = $temporary;
            }
        }
        return $record;
    }

    protected function setOperatorIdConditon($operator)
    {
        $q  = '';
        $q .= ' p.idoperator IN ( ';
        for ($i=0; $i < count($operator); $i++) { 
            $q .= $operator[$i];
            $q .= ($i < count($operator)-1) ? ', ' : '';
        }
        $q .= ' ) ';  
        return $q;  
    }

    public function getRecordPulsaAllOperator()
    {
        $data = array();
        foreach ($this->operator_pulsa as $key => $value) 
        {
            $records = $this->getRecordPulsaByOperatorId($value);
            $convert = array();
            foreach ($records as $row) {
                $tmp = array(
                    'kode_produk'   => $row['kodeproduk'],
                    'nama_terminal' => $row['namaterminal'],
                    'nama_produk'   => $row['namaproduk'],
                    'kode_aliansi'  => $row['aliastanpakode'],
                    'harga_produk'  => $row['hargabeli'],
                    'nominal'       => $row['nominal']
                );
                $convert[] = $tmp;
            }
            $data[$key] = $convert;
        }

        return $data;
    }

    public function getRecordPulsaByOperatorId($operator_id)
    {
        $q = ' SELECT mt.idproduk, mt.namaterminal, mt.namaproduk, mt.kodeproduk, mt.aliastanpakode, MIN(mt.hargabeli) As hargabeli, mt.nominal FROM produkterminal pt INNER JOIN ( ';
            $q .= ' SELECT p.idproduk,  t.namaterminal, p.namaproduk, p.kodeproduk, p.aliastanpakode, MIN(pt.hargabeli) AS hargabeli, p.nominal FROM produkterminal pt ';
            $q .= ' INNER JOIN produk p ON p.idproduk = pt.idproduk  INNER JOIN terminal t ON t.idterminal = t.idterminal ';
            $q .= ' WHERE t.aktif = 1 AND t.sibuk=0 AND t.putus=0 AND t.carakirim = 3 AND t.berlakusebagaisender=0 AND p.isgangguan = 0 AND ';
            $q .= ' p.iskartugosok = 0 AND p.isstokkosong = 0 AND pt.hargabeli > 0 AND pt.aktif = 1 AND   t.idterminal IN (427) AND ';

        if(is_array($operator_id) && count($operator_id) > 0)
            $q .= $this->setOperatorIdConditon($operator_id);           
        
        if(!is_array($operator_id) && $operator_id != null)
            $q .= ' p.idoperator IN ('.$operator_id.') ';

            $q .= ' GROUP BY p.kodeproduk  ORDER BY p.nominal ASC ';
        $q .= ' ) mt ON mt.idproduk = pt.idproduk  AND pt.hargabeli = mt.hargabeli GROUP BY mt.nominal ';    

        $select = $this->db->query($q);
        if($select)
            return ($select->num_rows() > 0) ? $select->result_array() : null;

        return false;
            
    }

    // Display Group Nominal
    public function getGroupNominalPulsaByOperatorId($operator_id)
    {
            
        $q = 'SELECT p.nominal FROM produkterminal pt INNER JOIN produk p ON p.idproduk = pt.idproduk  INNER JOIN terminal t ON t.idterminal = t.idterminal ';
        $q .= ' WHERE t.aktif = 1  AND t.sibuk=0 AND t.putus=0 AND t.carakirim = 3 AND t.berlakusebagaisender=0 AND p.isgangguan = 0 AND p.iskartugosok = 0  ';
        $q .=  ' AND p.isstokkosong = 0 AND pt.hargabeli > 0 AND pt.aktif = 1 AND  t.idterminal IN (427) AND ';

        $str = '';
        if(count($operator_id) > 1){
            $str = implode(',', $operator_id);
        }
        else{
            $str = $operator_id[0];
        }

        
        $q .= ' p.idoperator IN ('.$str.') ';
        $q .= ' GROUP BY p.nominal ORDER BY p.nominal ';
        
        $select = $this->db->query($q);
        return $select->result_array();
    }

    //Display Data Product By Group Nominal
    public function getRecordPulsaByOperatorAndNominalGroup($operator_id, $group_nominal)
    {
        $opt = $this->setOperatorIdConditon($operator_id);

        $q  = ' SELECT p.idproduk,  t.namaterminal, p.namaproduk, p.kodeproduk, p.aliastanpakode, pt.hargabeli, p.nominal FROM produkterminal pt ';
        $q .= ' INNER JOIN produk p ON p.idproduk = pt.idproduk  INNER JOIN terminal t ON t.idterminal = t.idterminal ';
        $q .= ' WHERE t.aktif = 1 AND t.sibuk=0 AND t.putus=0 AND t.carakirim = 3 AND t.berlakusebagaisender=0 AND p.isgangguan = 0 AND ';
        $q .= ' p.iskartugosok = 0 AND p.isstokkosong = 0 AND pt.hargabeli > 0 AND pt.aktif = 1 AND  t.idterminal IN (427) AND ';
        $q .= $opt . ' AND p.nominal = '.$group_nominal.' AND pt.hargabeli = ( ';
            $q .= ' SELECT MIN(pt.hargabeli) as hargabeli FROM produkterminal pt ';
            $q .= ' INNER JOIN produk p ON p.idproduk = pt.idproduk  INNER JOIN terminal t ON t.idterminal = t.idterminal ';
            $q .= ' WHERE t.aktif = 1 AND t.sibuk=0 AND t.putus=0 AND t.carakirim = 3 AND t.berlakusebagaisender=0 AND p.isgangguan = 0 AND ';
            $q .= ' p.iskartugosok = 0 AND p.isstokkosong = 0 AND pt.hargabeli > 0 AND pt.aktif = 1 AND  t.idterminal IN (427) AND ';
            $q .= $opt . ' AND p.nominal = '.$group_nominal.' ORDER BY p.nominal ';
        $q .= ' )  ORDER BY p.nominal ';

        $select = $this->db->query($q);
        if($select)
            return ($select->num_rows() > 0) ? $select->result_array() : null;
        
        return false;
    }

    //Display Record Product BY Product Code
    public function getRecordPulsaByProductCode($product_code)
    {
        $this->db->select('p.idproduk,  t.namaterminal, p.namaproduk, p.kodeproduk, p.aliastanpakode, pt.hargabeli, p.nominal')->from('produkterminal pt');
        $this->db->join('produk p', 'p.idproduk = pt.idproduk', 'left');
        $this->db->join('terminal t', 't.idterminal = pt.idterminal', 'left');
        $this->db->where('t.aktif', 1);
        $this->db->where('t.sibuk', 0);
        $this->db->where('t.putus', 0);
        $this->db->where('t.carakirim', 3);
        $this->db->where('t.berlakusebagaisender', 0);
        $this->db->where('p.isgangguan', 0);
        $this->db->where('p.iskartugosok', 0);
        $this->db->where('p.isstokkosong', 0);
        $this->db->where('pt.hargabeli > ', 0);
        $this->db->where('t.idterminal IN (427)');
        $this->db->where('p.kodeproduk', $product_code);

        $select = $this->db->get();
        if($select)
            return ($select->num_rows() > 0) ? $select->result_array() : null;
        
        return false;
    }

    public function getProductIRSByAllOperator($operator)
    {
        
    }
    
}

/* End of file Pulsa_model.php */
