<?php  defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . "./models/IRSConfig_model.php";

class Transaction_model extends IRSConfig_model 
{

    public function getMaxRecordTrx()
    {
        $select = $this->db->select('MAX(t.idtransaksi) As idtransaksi')->from($this->tbl_transaksi ." t")->get();
        return ($select->num_rows() > 0) ? $select->result()[0]->idtransaksi : 0;
    }

    public function getRecordStatusTrx($trxid)
    {
        $field = "t.statustransaksi, t.keterangan, t.kodeinboxcenter, t.sn";
        $select = $this->db->select($field)->from($this->tbl_transaksi. " t")->where("t.idtransaksi", $trxid)->get();

        if($select){
            return ($select->num_rows() > 0) ? $select->result() : null;
        }
        else{
            return false;
        }
       
    }

    public function getTransactionPriceProduct()
    {
        $result = $this->db->select('*')->from($this->tbl_transaksi . ' t')->where('t.statustransaksi', 1)->get();
        return (count($result) > 0) ? $result : false;
    }

}

/* End of file Transaction_model.php */
