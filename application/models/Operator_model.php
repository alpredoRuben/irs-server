<?php  defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "./models/IRSConfig_model.php";

class Operator_model extends IRSConfig_model 
{

    public function getRecordOperatorPulsa($operator_name=null)
    {
        return ($operator_name!=null) ? $this->operator_pulsa[$operator_name] : $this->operator_pulsa;
    }

    public function getRecordAllOperator()
    {
        $select = $this->db->select('*')->from($this->tbl_operator)->get();
        if($select)
            return ($select->num_rows() > 0) ? $select->result_array() : null;
        
        return false;
    }

    public function getOperatorByCategory($type)
    {
        switch($type)
        {
            case 'pulsa' : 
                return $this->operator_pulsa;
                break;

            case 'paket_data' : 
                return $this->operator_data;
                break;

            default:
                return false;
                break;
        }
    }


    

}

/* End of file Operator_model.php */
