<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class IRSConfig_model extends CI_Model {

    public $operator_pulsa = array(
        "telkomsel" => array(68),
        "xl"        => array(8, 34, 106),
        "axis"      => array(24),
        "esia"      => array(9),
        "indosat"   => array(32),
        "ceria"     => array(14),
        "flexy"     => array(6),
        "smartfren" => array(25),
        "three"     => array(10, 84)
    );


    public $operator_data = array(
        "telkomsel" => array(79),
        "xl"        => array(83,107),
        "axis"      => array(109),
        "indosat"   => array(104),
        "bolt"      => array(81),
        "smart"     => array(108),
        "three"     => array(85, 105)
    );

    /** Define Data Category  */
    protected $category = array(
        'pulsa' => array(),
        'paket_data' => array()
    );

    protected $tbl_produk = "produk";
    protected $tbl_transaksi  = "transaksi";
    protected $tbl_produk_terminal = "produkterminal";
    protected $tbl_operator = "operator";
    protected $tbl_terminal = "terminal";

    
    public function __construct()
    {
        parent::__construct();
        $this->category['pulsa'] = $this->operator_pulsa;
        $this->category['paket_data'] = $this->operator_data;
    }
    
}

/* End of file IRSConfig_model.php */
