<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class IRSTransaction extends IRS_api 
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("transaction_model");
    }

    protected function HTTPGetFormatMsg($no, $code, $trxid, $timeout=null)
    {
        $responses = $this->HttpGetTransaction($no, $code, $trxid, $timeout);
        $responses['clientid'] = $trxid;
        if(!empty($responses))
            return $responses;
        else
            return array("messages" => "No response data from server");
    }


    public function postHTTPPaymentTransaction()
    {
        $temporary_data = array();

        $input = json_decode(file_get_contents('php://input'), true);

        if(!isset($input['secret_key']) || empty($input['secret_key']));
            $this->setResponse(false, 'Silahkan Masukkan Secret Key');

        if(!isset($input['customer_number']) || empty($input['customer_number']))
            $this->setResponse(false, 'Silahkan Masukkan No. Pelanggan');

        if(!isset($input['product_code']) || empty($input['product_code']))
            $this->setResponse(false, 'Silahkan Masukkan Kode Produk');

        $transacts_id = $this->transaction_model->getMaxRecordTrx() + 1;
        $response_req = $this->HTTPGetFormatMsg($input['customer_number'], $input['product_code'], $transacts_id, null);

        $temporary_data['first_response_request'] = $response_req;

        if(isset($response_req['success']) && $response_req['success'] == true)
        {
            
            $status_terminal = $this->getStatusTrxTerminal($input['customer_number'], $input['product_code'], $transacts_id, 37);
            sleep(15);
            $status_database = $this->getStatusTrxDatabase($transacts_id);
            
            $time_process = date('Y-m-d H:i:s');

            $temporary_data['status_transaksi_terminal'] = $status_terminal;
            $temporary_data['status_transaksi_database'] = $status_database;

            $results = array(
                "transaksi_id" => $status_terminal['clientid'],
                "no_tujuan" => $status_terminal['tujuan'],
                "pesan_terminal" => $status_terminal['msg'],
            );

            if($status_database == false || $status_database == 0)
            {
                $this->setResponse(false, "Terjadi kesalahan dalam transaksi. Record data transaksi tidak ditemukan");
            }
            else
            {
                $results["status_transaksi"] = $status_database["status_trx"];
                $results["keterangan"] = $status_database["description"];
                $results["kode_inbox"] = $status_database["inbox_code"];
                $results["sn"] =  $status_database["sn"];
                $results["pesan_response"] = ($status_database["status_trx"] == "1") ? "Transaksi Berhasil" : "Transaksi Gagal";
                $results["waktu_transaksi"] = $time_process;

                $this->setResponse(true, $results, $status_terminal);
                //$this->setResponse(true, $results, $temporary_data);
            }
        }
        else
        {
            $results = array(
                'keterangan'    => 'Proses Transaksi Gagal',
                'pesan_respon'  => $response_req['msg'],
                'transaksi_id'  => $response_req['clientid'],
                'keterangan'    => 'Proses Transaksi Gagal',
                'tujuan'        => $response_req['tujuan'],
            );
            $this->setResponse(false, $results);
        }
    }

    public function getStatusTrxTerminal($no_customers, $product_code, $transacts_id, $timeout=null)
    {
        $response_req = $this->HTTPGetFormatMsg($no_customers, $product_code, $transacts_id, $timeout);
        return $response_req;
    }

    public function getStatusTrxDatabase($transacts_id)
    {
        $data_records = $this->transaction_model->getRecordStatusTrx($transacts_id);
        $temporary = array();

        if($data_records == false)
        {
            return false; //$this->setResponse(false, "Error To Get Record Data In Database IRS");
        }
        else 
        {
            if(count($data_records) > 0){
                foreach ($data_records as $v) {
                    $temporary["status_trx"]  = $v->statustransaksi;
                    $temporary["description"] = $v->keterangan;
                    $temporary["inbox_code"]  = $v->kodeinboxcenter;
                    $temporary["sn"]          = $v->sn;
                }
                return $temporary;
            }
            else{
                return 0; //$this->setResponse(false, "No Record IRS Data Product Active");
            }
               
        }
    }
    
    public function postXMLRPCPaymentTransaction()
    {
        $this->setJSONInput();
        $no_customers = $this->getCustomerNumber();
        $product_code = $this->getProductCode();
        $method_code  = $this->getMethodCode();
        $response_req = $this->HttpXMLPostTransaction($no_customers, $code_product, $method_code);
        $this->setResponse(false, $response_req);
    }


   

}

/* End of file IRSTransaksi_api.php */
