<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . './libraries/Encryption.php';

class IRSProduct extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("product_model");
        // load library irs
        $this->load->library('irs');
        $this->security = new Encryption();
    }

    /** Request List Product Pulsa By Operator */
    public function postListPulsaAllOperator()
    {
        $this->irs->setJSONInput();
        // $secret_key  = $this->getSecretKey();
        // if(!empty($secret_key) && $secret_key != null)
        // {
            $records = $this->product_model->getRecordPulsaAllOperator();
            $this->irs->setResponse(true, $records);
        // }
        // else
        // {
        //     $this->setResponse(false, "Please input IRS API Secret Key");
        // }
    }
    

    public function postListPulsaByOperator()
    {
        $this->setJSONInput();

        $operator_id = $this->getOperatorId();
        $secret_key  = $this->getSecretKey();

        
        if(!empty($secret_key) && $secret_key != null)
        {
            if(!is_array($operator_id) && $operator_id == null)
                $this->setResponse(false, "Please input Operator Id");

            if(is_array($operator_id) && count($operator_id) <= 0)
                $this->setResponse(false, "Please input Array Operator Id");

            $group_nominal = $this->product_model->getGroupNominalPulsaByOperatorId($operator_id);
            $record = array();
            if(is_array($group_nominal) && count($group_nominal) > 0){
                foreach ($group_nominal as $v) {
                    $tmp = $this->product_model->getRecordPulsaByOperatorAndNominalGroup($operator_id, $v['nominal']);
                    if(is_array($tmp) && count($tmp) > 0)
                    {
                        foreach ($tmp as $a) {
                            array_push($record, $a);
                        }
                    }
                    else
                        array_push($record, $tmp);
                }
            }
           
            if(is_array($record) && count($record) > 0)
            {
                $pulsa = array();
                foreach ($record as $v) 
                {
                    $tmp = array(
                        'kode_produk'   => $v['kodeproduk'],
                        'nama_terminal' => $v['namaterminal'],
                        'nama_produk'   => $v['namaproduk'],
                        'kode_aliansi'  => $v['aliastanpakode'],
                        'harga_produk'  => $v['hargabeli'],
                        'nominal'       => $v['nominal']
                    );
                    array_push($pulsa, $tmp);
                }

                $this->setResponse(true, $pulsa);
            }
            else
                $this->setResponse(false, 'List Product Pulsa Null');
        }
        else
            $this->setResponse(false, "Please input IRS API Secret Key");
    
    }

    public function postListProductByCode()
    {
        $this->setJSONInput();
        $secret_key  = $this->getSecretKey();
        $product_code = $this->getProductCode();

        if(!empty($secret_key) && $secret_key != null)
        {
            if(empty($product_code) && $operator_id == null)
                $this->setResponse(false, "Please Insert Product Code As Switch as IRS");

            $records = $this->product_model->getRecordPulsaByProductCode($product_code);

            if(is_array($records) && count($records) > 0)
            {
                $pulsa = array();
                foreach ($records as $v) 
                {
                    $tmp = array(
                        'kode_produk'   => $v['kodeproduk'],
                        'nama_terminal' => $v['namaterminal'],
                        'nama_produk'   => $v['namaproduk'],
                        'kode_aliansi'  => $v['aliastanpakode'],
                        'harga_produk'  => $v['hargabeli'],
                        'nominal'       => $v['nominal']
                    );
                    array_push($pulsa, $tmp);
                }

                $this->setResponse(true, $pulsa);
            }
            else
                $this->setResponse(false, 'List Product Pulsa Null');

        }
        else
        {
            $this->setResponse(false, "Please input IRS API Secret Key");
        }

    }

    public function postListProductByCategoryName()
    {
        $this->setJSONInput();
        $secret_key  = $this->getSecretKey();
        $category_name = $this->getCategoryName();

        if(!empty($secret_key) && $secret_key != null)
        {
            if(empty($category_name) && $category_name == null)
                $this->setResponse(false, "Silahkan Masukkan Kategory Produk Yang Sesuai Dengan Sistem IRS");

            $all_operator = (strtoupper($category_name) == 'PULSA') ? $this->pulsa : $this->paket_data; 
            
            $record_products = array();
            foreach ($all_operator as $opt) {
                $operator_id = $opt;
                $group_nominal = $this->product_model->getGroupNominalPulsaByOperatorId($operator_id);

                if(is_array($group_nominal) && count($group_nominal) > 0)
                {
                    foreach ($group_nominal as $v) 
                    {
                        $tmp = $this->product_model->getRecordPulsaByOperatorAndNominalGroup($operator_id, $v['nominal']);
                        
                        if(is_array($tmp) && count($tmp) > 0)
                        {
                            foreach ($tmp as $a) {
                                array_push($record_products, $a);
                            }
                        }
                        else
                            array_push($record_products, $tmp);
                    }
                }

                
            }


            if(is_array($record_products) && count($record_products) > 0)
            {
                $pulsa = array();
                foreach ($record_products as $v) 
                {
                    $tmp = array(
                        'kode_produk'   => $v['kodeproduk'],
                        'nama_terminal' => $v['namaterminal'],
                        'nama_produk'   => $v['namaproduk'],
                        'kode_aliansi'  => $v['aliastanpakode'],
                        'harga_produk'  => $v['hargabeli'],
                        'nominal'       => $v['nominal']
                    );
                    array_push($pulsa, $tmp);
                }

                $record_products = $pulsa;
            }

            if(count($record_products) > 0)
                $this->setResponse(true, $record_products);
            else
                $this->setResponse(false, 'Daftar Produk Berdasarkan Jenis Produk '.$category_name. ' Kosong');
        }
        else
        {
            $this->setResponse(false, "Please input IRS API Secret Key");
        }

    }


    public function postListAllProductByCategoryAndOperator()
    {
        $this->setJSONInput();
        //$secret_key  = $this->getSecretKey();
        $category_name = $this->getCategoryName();
        $operator_name = $this->getOperatorName();

        // if(!empty($secret_key) && $secret_key != null)
        // {
            if(empty($category_name) && $category_name == null)
                $this->setResponse(false, "Silahkan Masukkan Kategory Produk Yang Sesuai Dengan Sistem IRS");

            if(empty($operator_name) && $operator_name == null)
                $this->setResponse(false, "Silahkan Masukkan Nama Operator Yang Sesuai Dengan Sistem IRS");

            $all_category = (strtoupper($category_name) == 'PULSA') ? $this->pulsa : $this->paket_data; 
            $operator_id = $all_category[strtolower($operator_name)];

            $nominal_group = $this->product_model->getGroupNominalPulsaByOperatorId($operator_id);
            $record_products = array();
            
            if(is_array($nominal_group) && count($nominal_group) > 0)
            {
                foreach ($nominal_group as $v) 
                {
                    $tmp = $this->product_model->getRecordPulsaByOperatorAndNominalGroup($operator_id, $v['nominal']);
                    
                    if(is_array($tmp) && count($tmp) > 0)
                    {
                        foreach ($tmp as $a) {
                            array_push($record_products, $a);
                        }
                    }
                    else
                        array_push($record_products, $tmp);
                }
            }


            if(is_array($record_products) && count($record_products) > 0)
            {
                $pulsa = array();
                foreach ($record_products as $v) 
                {
                    $tmp = array(
                        'kode_produk'   => $v['kodeproduk'],
                        'nama_terminal' => $v['namaterminal'],
                        'nama_produk'   => $v['namaproduk'],
                        'kode_aliansi'  => $v['aliastanpakode'],
                        'harga_produk'  => $v['hargabeli'],
                        'nominal'       => $v['nominal']
                    );
                    array_push($pulsa, $tmp);
                }

                $record_products = $pulsa;
            }

            if(count($record_products) > 0)
                $this->setResponse(true, $record_products);
            else
                $this->setResponse(false, 'Daftar Produk Berdasarkan Jenis Produk '.$category_name. ' Kosong');

        // }
        // else{
        //     $this->setResponse(false, 'Please input IRS API Secret Key');
        // }

    }

}

/* End of file IRSProduct_api.php */
