<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class IRSAuthorize extends IRS_api {

    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getEncryptionAPISecretKeyIRS()
    {
        $api_secret = $this->getEncryptApiSecret();
        $this->setResponse(true, array('secret_key' => $api_secret));
    }
    
    public function getDecryptionAPISecretKeyIRS($secret)
    {
        $decrypt = $this->getDecryptApiSecret($secret);
        $this->setResponse(true, array('secret_key' => $decrypt));
    }
}

/* End of file IRSAuthorize.php */
