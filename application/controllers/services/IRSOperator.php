<?php defined('BASEPATH') OR exit('No direct script access allowed');

class IRSOperator extends IRS_api 
{    
    public function __construct()
    {
        parent::__construct();
        $this->load->model("operator_model"); 
    }

    /** Function To Get List Operator Pulsa */
    public function postListOperatorProduct()
    {
        $this->setJSONInput();
        $secret_key = $this->getSecretKey();
        $category_id = $this->getCategoryId();

        if($secret_key != null && !empty($secret_key))
        {
            if($this->getValidateSecretKey($secret_key))
            {
                $operator = $this->operator_model->getOperatorByCategory($category_id);
                $this->setResponse(true, $operator);
            }
            else 
                $this->setResponse(false, "Invalid Input IRS API Secret Key");
            
        }
        else
            $this->setResponse(false, "Please insert IRS API Secret Key");
    }

    /** Function To Get List All Operator */
    public function postListAllOperator()
    {
        $this->setJSONInput();
        $secret_key = $this->getSecretKey();
        
        if($secret_key != null && !empty($secret_key))
        {
            if($this->getValidateSecretKey($secret_key))
            {
                $records = $this->operator_model->getRecordAllOperator();
                $operator = array();
                foreach ($records as $v) {
                    $tmp = array(
                        'operator_id'   => $v['IDOPERATOR'],
                        'operator_name' => $v['NAMAOPERATOR'],
                        'min_digit'     => $v['mindigit'],
                        'max_digit'     => $v['maxdigit']
                    );
                    array_push($operator, $tmp);
                }
                $this->setResponse(true, $operator);
            }
            else 
                $this->setResponse(false, "Invalid Input IRS API Secret Key");
            
        }
        else
            $this->setResponse(false, "Please insert IRS API Secret Key");
    }

}

/* End of file IRSOperator_api.php */
